![Picture](screenshot.png)

# Krita Scratchpad Docker

This is a Python docker plugin you can load into Krita. It creates a mini canvas docker you can put on the side.


It has the following features
1. Load images for scratchpad
1. Save images as scratchpad
1. Change modes of how the scratchpad works (painting, panning, color picking)
1. the canvas size is fluid, so you kind of get an infinite canvas
1. change canvas color and clear background


## Requirements and installation

This is a newer API for Krita. You will need Krita 5.0 to run it. You will have to download this from the nightly builds if Krita 5.0 isn't out yet. 

To install, download the repository as a ZIP file. Then in Krita, go to the main menu Scripts > Load plugin and pick the ZIP file.

You will have to restart to see the docker from the dockers list in the main toolbar.

