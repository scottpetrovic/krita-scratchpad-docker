"""
Copyright (c) 2020 Scott Petrovic <scottpetrovic@gmail.com>

This file is part of the Scratchpad Docker, a Krita Python plugin.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the CPMT.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This is a docker that creates a portable canvas to paint on
"""


import krita
from .scratchpad import ScratchpadDocker


Application.addDockWidgetFactory(
    krita.DockWidgetFactory("scratchpaddocker",
                            krita.DockWidgetFactoryBase.DockRight,
                            ScratchpadDocker))
