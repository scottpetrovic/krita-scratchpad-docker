"""
Copyright (c) 2020 Scott Petrovic <scottpetrovic@gmail.com>

This file is part of the Scratchpad Docker, a Krita Python plugin.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the CPMT.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This is a docker that creates a portable canvas to paint on
"""


from krita import DockWidget, DockWidgetFactory, DockWidgetFactoryBase, Scratchpad
import krita
from PyQt5.QtCore import Qt, QTimer, QSize
from PyQt5.QtWidgets import (QColorDialog, QSpacerItem, QSizePolicy, QButtonGroup, QWidget, QScrollArea, QDialogButtonBox, QDialog, QMessageBox, QComboBox, QVBoxLayout, QHBoxLayout, QFileDialog, QLabel, QToolButton, QAction, QPushButton, QSpinBox)
from PyQt5.QtGui import QImage, QPixmap, QIcon, QColor, QImageWriter


class ScratchpadDocker(krita.DockWidget):


    canvasColor = QColor(255,255,255)


    def __init__(self):
        super(ScratchpadDocker, self).__init__()
        self.setWindowTitle("Scratchpad")

        self.baseWidget = QWidget()
        self.vbox = QVBoxLayout()
        self.vbox.setContentsMargins(2, 2, 2, 2)
        self.baseWidget.setLayout(self.vbox)
        self.setWidget(self.baseWidget)
        
        self.newScratchpad = 0 # where scratchpad widget will exist once a canvas exists
        #self.hasScratchPad = False
        self.setDefaultModeTimer = QTimer()

        
    def setupUIControls(self):

        self.toolButtonGroup = QButtonGroup()

        # buttons to do things
        self.buttonPaintMode = QToolButton()
        self.buttonPaintMode.setIcon(Application.icon("krita_tool_freehand"))
        self.buttonPaintMode.setCheckable(True)      
        self.buttonPaintMode.setAutoRaise(True)                      
        self.buttonPaintMode.setToolTip("Paint Mode")

        self.buttonPanMode = QToolButton()
        self.buttonPanMode.setIcon(Application.icon("tool_pan"))
        self.buttonPanMode.setCheckable(True)
        self.buttonPanMode.setAutoRaise(True)  
        self.buttonPanMode.setToolTip("Pan Mode")

        self.buttonColorPickMode = QToolButton()
        self.buttonColorPickMode.setIcon(Application.icon("krita_tool_color_sampler"))
        self.buttonColorPickMode.setCheckable(True)
        self.buttonColorPickMode.setAutoRaise(True)  
        self.buttonColorPickMode.setToolTip("Color Pick Mode")

        self.buttonClearScratchpad = QToolButton()
        self.buttonClearScratchpad.setAutoRaise(True)
        self.buttonClearScratchpad.setIcon(Application.icon("deletelayer"))

        self.buttonBackgroundColor = QToolButton()
        self.buttonBackgroundColor.setIcon(Application.icon("tool_pan"))

        # buttons for loading and saving
        self.loadScratchpadButton = QToolButton()
        self.loadScratchpadButton.setAutoRaise(True)  
        self.loadScratchpadButton.setIcon(Application.icon("document-open"))

        self.saveScratchpadImageButton = QToolButton()
        self.saveScratchpadImageButton.setAutoRaise(True)
        self.saveScratchpadImageButton.setIcon(Application.icon("document-save"))
        
        
        # put buttons in new horizontal widget/layout and add to top level vbox
        self.horizontalToolbarWidget = QWidget()
        self.toolHbox = QHBoxLayout()
        self.toolHbox.setContentsMargins(5, 5, 5, 5)
        self.horizontalToolbarWidget.setLayout(self.toolHbox)

        self.vbox.addWidget(self.horizontalToolbarWidget)

        # add buttons to toolbar
        self.toolHbox.addWidget(self.loadScratchpadButton)
        self.toolHbox.addWidget(self.saveScratchpadImageButton)
        self.toolHbox.addStretch()

        self.toolButtonGroup.addButton(self.buttonPaintMode)
        self.toolButtonGroup.addButton(self.buttonPanMode)
        self.toolButtonGroup.addButton(self.buttonColorPickMode)
        self.toolHbox.addWidget(self.buttonPaintMode)
        self.toolHbox.addWidget(self.buttonPanMode)
        self.toolHbox.addWidget(self.buttonColorPickMode)


        self.toolHbox.addStretch()
        self.toolHbox.addWidget(self.buttonBackgroundColor)
        self.toolHbox.addWidget(self.buttonClearScratchpad)


        # set paint mode by default (this needs to later turn into a function)
        self.setBackgroundColorForButton(self.canvasColor)
        self.buttonBackgroundColor.clicked.connect( self.getBackgroundColorForButton )
           
    
        self.newScratchpad.setModeManually(True) # allows mode to be set by GUI controls ## TODO: Why is not setting this crashing?
        self.newScratchpad.linkCanvasZoom(False)

        self.buttonClearScratchpad.clicked.connect( self.clearScratchpad )

        self.buttonPaintMode.clicked.connect( self.changeToPaintingMode  )
        self.buttonPanMode.clicked.connect( self.changeToPanMode )
        self.buttonColorPickMode.clicked.connect( self.changeToColorPickMode )

        self.loadScratchpadButton.clicked.connect( self.loadScratchpadFromImage )
        self.saveScratchpadImageButton.clicked.connect( self.saveScratchpadImage )

        # we want the painting mode to be set by default
        # HACK: if set the mode when the UI is created, the paint mode is not set for some reason
        # we are going to constantly set it and it will eventually set.
        # if any of the other modes are clicked...this timer will stop
        self.setDefaultModeTimer.timeout.connect(self.changeToPaintingMode)
        self.setDefaultModeTimer.start(1000)


    def loadScratchpadFromImage(self):
        #print("loading scratch pad")
        fname = QFileDialog.getOpenFileName(self, 'Open file', '~',"Image files (*.jpg *.gif *.png)")    

        if fname:
            imageToLoad = QImage(fname[0])
            #print(str(imageToLoad.height() ) )    
    
        self.newScratchpad.loadScratchpadImage(imageToLoad)
    
    def saveScratchpadImage(self):
        #print("saving scratchpad")
        scratchPadImage = QImage(self.newScratchpad.copyScratchpadImageData() ) 
        #print(str(scratchPadImage.height()) + "  " + str(scratchPadImage.width()) )
    
        # save QImage to disk
        fsavename = QFileDialog.getSaveFileName(self, 'Save file', 'scratchpad.png',"Image files (*.png)")   
    
        if fsavename:
            scratchPadImage.save(fsavename[0]);  


    # define functions
    def clearScratchpad(self):
        self.newScratchpad.clear()
    
    def changeToPaintingMode(self):
        self.newScratchpad.setMode("painting")
 
    def changeToPanMode(self):
        self.setDefaultModeTimer.stop() # hack for setting default to paint mode
        self.newScratchpad.setMode("panning")

    def changeToColorPickMode(self):
        self.setDefaultModeTimer.stop() # hack for setting default to paint mode
        self.newScratchpad.setMode("colorsampling") 
   

    def setBackgroundColorForButton(self, colorToSet):   
        colorPixmap = QPixmap(20, 20)
        colorPixmap.fill(colorToSet)
        self.canvasColor = colorToSet
        buttonColorIcon = QIcon()
        buttonColorIcon.addPixmap(colorPixmap)
        self.buttonBackgroundColor.setIcon(buttonColorIcon)
        self.newScratchpad.setFillColor(self.canvasColor)        
       
        
    def getBackgroundColorForButton(self):
        global canvasColor # this makes sure to reference global variable
        canvasColor = QColorDialog.getColor() # grab color from dialog and display it on the button 
        self.setBackgroundColorForButton(canvasColor)

    def setWidgetEnabled(self, value):
        # if widget becomes enabled or disabled...we need to update the various widgets UI
        self.newScratchpad.setEnabled(value)
        self.buttonPaintMode.setEnabled(value)
        self.buttonPanMode.setEnabled(value)
        self.buttonColorPickMode.setEnabled(value)        
        self.buttonClearScratchpad.setEnabled(value)
        self.buttonBackgroundColor.setEnabled(value)
        self.loadScratchpadButton.setEnabled(value)
        self.saveScratchpadImageButton.setEnabled(value)

    def canvasChanged(self, canvas):
   
        # when we close the last canvas or krita, this will not exist, so don't try to manipulate it
        if canvas:     
            #print('canvas changed!!')
            if canvas.view():
                #print("view exists")
                
                # we only want one scratchpad. Create a scratchpad if it does not exist. otherwise just load what is there
                self.activeDocument = canvas.view().document()    # deprecated!!!!!!
                self.activeView = canvas.view()         

                if self.newScratchpad == 0:  
                    #self.hasScratchPad = True     
                    
                    #print('creating a new scratchpad')  
                    # self.newScratchpad = self.activeDocument.createScratchpad()
                    self.canvasColor = QColor(180,180,180)   
                    self.newScratchpad = Scratchpad(self.activeView, self.canvasColor)
                    
                    self.newScratchpad.setFillColor(self.canvasColor)
                    self.newScratchpad.clear()

                    self.expandSizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)

                    self.newScratchpad.setSizePolicy(self.expandSizePolicy)
                    self.vbox.addWidget(self.newScratchpad)
                  
                    self.setupUIControls()
                else:
                    self.setWidgetEnabled(True)

        else:
            #print('no canvas exists')
            if self.newScratchpad:
                self.setWidgetEnabled(False)

